﻿using System;
using System.Text;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using System.Threading;

namespace RedditFetch
{
    class Program
    {
        static void Main(string[] args)
        {
            SendToAPI(GetDayNews());

        }

        private static Rootobject GetDayNews()
        {
            string newsfeed = string.Empty;
            using (var client = new HttpClient())
            {
                var response = client.GetAsync("https://www.reddit.com/r/worldnews/hot.json?limit=20").Result;
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = response.Content;
                    newsfeed = responseContent.ReadAsStringAsync().Result;
                }
            }
            Rootobject resultitems = new JavaScriptSerializer().Deserialize<Rootobject>(newsfeed);
            return resultitems;
        }

        private static void SendToAPI(Rootobject feed)
        {
            Uri myapi = new Uri("http://localhost:8000/api/WorldNews/");
            HttpClient httpClient = new HttpClient();
            foreach (var item in feed.data.children)
            {
                string title = item.data.title ?? "empty";
                string link = item.data.url ?? "empty";
                string score = item.data.score.ToString() ?? "empty";
                string date = DateTime.Today.ToString();
                string flair = item.data.link_flair_text ?? "empty";
                string jsonstr = "{\"Title\":\"" + title + "\",\"Link\":\"" + link + "\",\"Score\":\"" + score + "\",\"Date\":\"" + date + "\",\"Flair\":\"" + flair + "\"}";
                var content = new StringContent(jsonstr.ToString(), Encoding.UTF8, "application/json");

                var result =  httpClient.PostAsync(myapi, content).Result;
            }
        }

        public class Rootobject
        {
            public string kind { get; set; }
            public Data data { get; set; }
        }

        public class Data
        {
            public string after { get; set; }
            public int dist { get; set; }
            public string modhash { get; set; }
            public string whitelist_status { get; set; }
            public Child[] children { get; set; }
            public object before { get; set; }
        }

        public class Child
        {
            public string kind { get; set; }
            public Data1 data { get; set; }
        }

        public class Data1
        {
            public string subreddit_id { get; set; }
            public object approved_at_utc { get; set; }
            //public bool send_replies { get; set; }
            public object mod_reason_by { get; set; }
            public object banned_by { get; set; }
            public object num_reports { get; set; }
            public object removal_reason { get; set; }
            public string subreddit { get; set; }
            public object selftext_html { get; set; }
            public string selftext { get; set; }
            public object likes { get; set; }
            public object suggested_sort { get; set; }
            public object[] user_reports { get; set; }
            public object secure_media { get; set; }
            //public bool is_reddit_media_domain { get; set; }
            //public bool saved { get; set; }
            public string id { get; set; }
            public object banned_at_utc { get; set; }
            public object mod_reason_title { get; set; }
            public object view_count { get; set; }
            public bool archived { get; set; }
            //public bool clicked { get; set; }
            //public bool no_follow { get; set; }
            public string author { get; set; }
            public int num_crossposts { get; set; }
            public string link_flair_text { get; set; }
            public object[] mod_reports { get; set; }
            //public bool can_mod_post { get; set; }
            //public bool is_crosspostable { get; set; }
            //public bool pinned { get; set; }
            public int score { get; set; }
            public object approved_by { get; set; }
            //public bool over_18 { get; set; }
            public object report_reasons { get; set; }
            public string domain { get; set; }
            //public bool hidden { get; set; }
            public string thumbnail { get; set; }
            //public bool edited { get; set; }
            public string link_flair_css_class { get; set; }
            public object author_flair_css_class { get; set; }
            //public bool contest_mode { get; set; }
            public int gilded { get; set; }
            public int downs { get; set; }
            //public bool brand_safe { get; set; }
            public Secure_Media_Embed secure_media_embed { get; set; }
            public Media_Embed media_embed { get; set; }
            public object author_flair_text { get; set; }
            //public bool stickied { get; set; }
            //public bool visited { get; set; }
            //public bool can_gild { get; set; }
            //public bool is_self { get; set; }
            public string parent_whitelist_status { get; set; }
            public string name { get; set; }
            //public bool spoiler { get; set; }
            public string permalink { get; set; }
            public string subreddit_type { get; set; }
            //public bool locked { get; set; }
            //public bool hide_score { get; set; }
            public float created { get; set; }
            public string url { get; set; }
            public string whitelist_status { get; set; }
            //public bool quarantine { get; set; }
            public int subreddit_subscribers { get; set; }
            public float created_utc { get; set; }
            public string subreddit_name_prefixed { get; set; }
            public int ups { get; set; }
            public object media { get; set; }
            public int num_comments { get; set; }
            public string title { get; set; }
            public object mod_note { get; set; }
            //public bool is_video { get; set; }
            public object distinguished { get; set; }
        }

        public class Secure_Media_Embed
        {
        }

        public class Media_Embed
        {
        }
    }
}
